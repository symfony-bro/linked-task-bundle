<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\LinkedTaskBundle\EventDispatcher;


use Symfony\Component\EventDispatcher\Event;
use SymfonyBro\TaskBundle\Model\TaskInterface;

class TasksLinkedEvent extends Event
{
    /**
     * @var TaskInterface
     */
    private $sourceTask;

    /**
     * @var TaskInterface
     */
    private $linkedTask;

    /**
     * TasksLinkedEvent constructor.
     * @param TaskInterface $sourceTask
     * @param TaskInterface $linkedTask
     */
    public function __construct(TaskInterface $sourceTask, TaskInterface $linkedTask)
    {
        $this->sourceTask = $sourceTask;
        $this->linkedTask = $linkedTask;
    }

    /**
     * @return TaskInterface
     */
    public function getSourceTask(): TaskInterface
    {
        return $this->sourceTask;
    }

    /**
     * @return TaskInterface
     */
    public function getLinkedTask(): TaskInterface
    {
        return $this->linkedTask;
    }
}