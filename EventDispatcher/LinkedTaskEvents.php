<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\LinkedTaskBundle\EventDispatcher;


final class LinkedTaskEvents
{
    public const LINKED = 'symfony-bro.linked_task.linked';
}