<?php

namespace SymfonyBro\LinkedTaskBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use SymfonyBro\LinkedTaskBundle\DependencyInjection\Compiler\SetupLinkProviderPass;

class SymfonyBroLinkedTaskBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new SetupLinkProviderPass());
    }
}
