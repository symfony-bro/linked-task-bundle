<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\LinkedTaskBundle\Model;


use SymfonyBro\TaskBundle\Model\TaskInterface;

class LinkedTaskProvider implements LinkedTaskProviderInterface
{
    /**
     * @var LinkedTaskResolverInterface[]
     */
    private $resolvers = [];

    /**
     * @param TaskInterface $task
     * @return TaskInterface[]
     */
    public function findLinked(TaskInterface $task): array
    {
        $linked = [[]];

        foreach ($this->resolvers as $resolver) {
            if ($resolver->supports($task)) {
                $linked[] = $resolver->resolve($task);
            }
        }

        return \array_merge(...$linked);
    }

    public function addResolver(LinkedTaskResolverInterface $resolver)
    {
        if (\in_array($resolver, $this->resolvers, true)) {
            throw new \InvalidArgumentException('Resolver already added');
        }

        $this->resolvers[] = $resolver;
    }
}