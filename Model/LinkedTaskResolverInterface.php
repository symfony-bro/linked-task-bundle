<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\LinkedTaskBundle\Model;


use SymfonyBro\TaskBundle\Model\TaskInterface;

interface LinkedTaskResolverInterface
{
    /**
     * @param TaskInterface $task
     * @return TaskInterface[]
     */
    public function resolve(TaskInterface $task): array;

    public function supports(TaskInterface $task): bool;
}