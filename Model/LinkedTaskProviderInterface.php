<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\LinkedTaskBundle\Model;


use SymfonyBro\TaskBundle\Model\TaskInterface;

interface LinkedTaskProviderInterface
{
    /**
     * @param TaskInterface $task
     * @return TaskInterface[]
     */
    public function findLinked(TaskInterface $task): array;
}