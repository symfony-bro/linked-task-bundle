<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\LinkedTaskBundle\Model;


use SymfonyBro\TaskBundle\Model\TaskInterface;

interface LinkedTaskInterface extends TaskInterface
{
    public function getSourceTask(): ?TaskInterface;

    public function setSourceTask(?TaskInterface $task);
}