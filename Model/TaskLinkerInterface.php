<?php
/**
 * @author Artem Dekhtyar <m@artemd.ru>
 * @author Pavel Stepanets <pahhan.ne@gmail.com>
 */

namespace SymfonyBro\LinkedTaskBundle\Model;


use SymfonyBro\TaskBundle\Model\TaskInterface;

interface TaskLinkerInterface
{
    public function link(TaskInterface $sourceTask, TaskInterface $task): void;
}